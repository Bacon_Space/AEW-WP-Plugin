<?php
/**
 * Plugin Name: Impact Wordpress Plugin
 * Plugin URI: https://gitlab.com/Bacon_Space/Impact-WP-Plugin
 * Description: This is a plugin that provides information on how to watch Impact! Wrestling. Shortcode [impact]
 * Version: 1.0.0
 * Author: Bacon_Space
 * Author URI: https://twitter.com/Bacon_Space
 */


function aew() {
   
    // Additional information about where to watch AEW
    $output .= "<h4>Watch All Elite Wrestling</h4>";
    $output .= "<ul>";
	    $output .= "<li>Youtube: Monday's & Tuesday 7pm ET on <a href='https://www.youtube.com/aew' target='_blank'>AEW Dark Elevation & AEW Dark</a></li>";
    $output .= "<li>USA: Wednesdays 8pm ET on <a href='https://www.tbs.com/allelitewrestling' target='_blank'>TBS</a> & 10pm ET on <a href='https://www.tbs.com/allelitewrestling' target='_blank'>AEW All Acess</a></li>";
	    $output .= "<li>USA: Fridays 10pm ET on <a href='https://www.tntdrama.com/shows/all-elite-wrestling-rampage' target='_blank'>TNT</a></li>";
    $output .= "<li>Canada: Wednesdays 8pm ET on <a href='https://www.tsn.ca/live' target='_blank'>TSN</a></li>";
	$output .= "<li>Canada: Friday 10pm ET on <a href='https://www.tsn.ca/live' target='_blank'>TSN</a></li>";
    $output .= "<li>UK: Thursdays 1am GMT on <a href='https://www.fite.tv/join/aew-plus/' target='_blank'>FITE TV</a></li>";
    $output .= "</ul>";
    
    return $output;
}

add_shortcode('aew', 'aew');
